package com.example.exception.handler;

import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.example.exception.AlreadyExistException;
import com.example.response.model.CommonResponse;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

	@Autowired
	private MessageSource messageSource;

	@Override
	protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers,
			HttpStatus status, WebRequest request) {
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
				.body(this.errorResponse(ex, status, "something-wrong", Boolean.TRUE));
	}

	private CommonResponse errorResponse(Exception ex, HttpStatus status, String messageProperty,
			boolean printException) {

		return new CommonResponse(status.value(), Boolean.FALSE,
				messageProperty + (printException ? " Error: " + ex.getLocalizedMessage() : ""));
	}

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {

		String data = ex.getBindingResult().getAllErrors().stream()
				.map(error -> this.messageSource.getMessage(error, LocaleContextHolder.getLocale()))
				.collect(Collectors.joining(", "));

		return ResponseEntity.status(status.value())
				.body(new CommonResponse(status.value(), Boolean.FALSE, data, null));
	}

	public ResponseEntity<CommonResponse> handleErrors(BindingResult bindingResult) {
		FieldError fieldError = bindingResult.getFieldError();
		String error = null != fieldError
				? null != fieldError.getDefaultMessage() ? fieldError.getDefaultMessage() : "something-wrong"
				: "something-wrong";
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new CommonResponse(HttpStatus.BAD_REQUEST.value(),
				Boolean.FALSE, null != error && !"".equals(error) ? error : "something-wrong", null));
	}

	@ExceptionHandler(AlreadyExistException.class)
	protected ResponseEntity<Object> alreadyExists(AlreadyExistException ex, WebRequest request) {

		return ResponseEntity.status(HttpStatus.OK).body(new CommonResponse(HttpStatus.OK.value(), Boolean.FALSE,
				ex.getMessage() == null ? "Data already exist" : ex.getMessage()));

	}
}

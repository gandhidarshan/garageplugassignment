package com.example.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.model.Orders;
import com.example.repository.OrdersRepository;
import com.example.service.OrdersService;

@Service
public class OrdersServiceImpl implements OrdersService {

	@Autowired
	private OrdersRepository ordersRepository;
	
	@Override
	public List<Orders> getOrdersByCustomerId(Long id) {
		return ordersRepository.findByCustIdOrderByCreatedTimeDesc(id);
	}

	@Override
	public Orders addOrder(Orders order) {
		return ordersRepository.save(order);
	}

}

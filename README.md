Technologies : Spring Boot (2.3.4)
Spring Data JPA
IDE : Spring Tool Suite 4
Version : Version: 4.3.0.RELEASE

Database : Mysql
DB Schema : orderManagement

Note: I have added API postman collection Order Management collection.json under the
project.

Step To Deploy :
1. application.properties file changes the database configuration your local system
accordingly
2. mvn clean install
3. mvn spring-boot:run
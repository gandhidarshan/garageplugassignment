package com.example.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.exception.AlreadyExistException;
import com.example.exception.NoDataFoundException;
import com.example.model.Customer;
import com.example.request.model.CustomerRequest;
import com.example.response.model.CommonResponse;
import com.example.service.CustomerService;

@RestController
public class CustomerRestController {
	private static Logger log = LoggerFactory.getLogger(CustomerRestController.class.getName());

	@Autowired
	private CustomerService customerService;

	@PostMapping("/add/customer")
	public ResponseEntity<CommonResponse> createCustomer(@Valid @RequestBody CustomerRequest customerRequest) {
		log.info("Create Customer Calling");
		Map<String, Object> customerModelMap = new HashMap<>();

		if (customerService.getCustomerByEmail(customerRequest.getEmail()) != null)
			throw new AlreadyExistException("Custoemr with Email already exist");

		Customer customer = new Customer();
		BeanUtils.copyProperties(customerRequest, customer);

		customer = customerService.addCustomer(customer);

		customerModelMap.put("customer", customer);
		log.info("Customer Created successfully");

		return ResponseEntity.status(HttpStatus.OK).body(
				new CommonResponse(HttpStatus.OK.value(), true, "Customer inserted successfully", customerModelMap));
	}

	@GetMapping("/get/all/customers")
	public ResponseEntity<CommonResponse> getAllCustomer() {
		Map<String, Object> customerModelMap = new HashMap<>();

		List<Customer> customerList = customerService.getAllCustomers();
		if(customerList.isEmpty())
			throw new NoDataFoundException("No Customers exist");
		
		customerModelMap.put("customers", customerList);
		
		return ResponseEntity.status(HttpStatus.OK).body(
				new CommonResponse(HttpStatus.OK.value(), true, "Fetched all customers data", customerModelMap));
		
	}
}

package com.example.service;

import java.util.List;

import com.example.model.Customer;

public interface CustomerService {

	public Customer addCustomer(Customer customer);
	public Customer getCustomerById(Long id);
	public Customer getCustomerByEmail(String email);
	public List<Customer> getAllCustomers();
	public List<Object[]> getCustomersByTotalTransaction(int total);
	
}

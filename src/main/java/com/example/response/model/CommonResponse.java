package com.example.response.model;

import java.util.Map;

import lombok.Data;

@Data
public class CommonResponse {
	private int status;
	private Boolean success;
	private String message;
	private Map<String, Object> data;
	
	public CommonResponse(int status, boolean success, String message, Map<String, Object> data) {
		super();
		this.status = status;
		this.success = success;
		this.message = message;
		this.data = data;
	}
	
	public CommonResponse(int status, boolean success, String message) {
		super();
		this.status = status;
		this.success = success;
		this.message = message;
	}
	
	public CommonResponse() {
		super();
	}
	
	public CommonResponse(int status, String message, Map<String, Object> data) {
		super();
		this.status = status;
		this.message = message;
		this.data = data;
	}

	public CommonResponse(int status, String message) {
		super();
		this.status = status;
		this.message = message;
	}
}

package com.example.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.model.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {

	Optional<Customer> findByEmail(String email);

	@Query(value = "SELECT * FROM (SELECT COUNT(*) AS total,cust_id as custId,email FROM orders LEFT JOIN customer on orders.cust_id = customer.id GROUP BY cust_id) AS temp WHERE temp.total =:total ", nativeQuery = true)
	List<Object[]> findCustomerByTotalOrder(int total);
}

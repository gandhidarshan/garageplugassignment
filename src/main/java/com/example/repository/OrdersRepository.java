package com.example.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.model.Orders;

@Repository
public interface OrdersRepository extends JpaRepository<Orders,Long>{
	
	@Query(value ="SELECT * FROM orders where cust_id=:customerId order by created_time desc",nativeQuery = true)
	List<Orders> findByCustIdOrderByCreatedTimeDesc(Long customerId);
	
}

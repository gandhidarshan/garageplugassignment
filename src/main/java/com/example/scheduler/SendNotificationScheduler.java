package com.example.scheduler;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.example.service.CustomerService;
import com.example.utility.CommonUtil;

@Component
public class SendNotificationScheduler {
	private static Logger log = LoggerFactory.getLogger(SendNotificationScheduler.class.getName());
	
	@Autowired
	private CustomerService customerService;

	@Autowired
	private CommonUtil commonUtil;
	
	@Scheduled(cron = "0 */1 * * * ?")
	public void sendCustomerNotification() {
		log.info("Send Notification to Customer");
		
		List<Object[]> customersListRegular = customerService.getCustomersByTotalTransaction(1);
		if(!customersListRegular.isEmpty()) {
			customersListRegular.stream().forEach(Object ->{
				commonUtil.sendEmail(Object[2].toString(),"You have placed 9 orders with us. Buy one more stuff and you will be promoted to Gold customer and enjoy 10% discounts!");
			});
		}
		
		List<Object[]> customersListGold = customerService.getCustomersByTotalTransaction(19);
		if(!customersListGold.isEmpty()) {
			customersListGold.stream().forEach(Object ->{
				commonUtil.sendEmail(Object[2].toString(),"You have placed 19 orders with us. Buy one more stuff and you will be promoted to Platinum customer and enjoy 20% discounts!");
			});
		}
		
		
	}
}

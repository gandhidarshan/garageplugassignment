package com.example.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@Entity
@Table(name = "orders")
public class Orders implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id", unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long orderrId;
	
	@Column(nullable = false)
	private BigDecimal amount;
	private Double discountInPercentage = 0.0;
	private BigDecimal discountAmount = new BigDecimal(0.0);
	private BigDecimal totalAmount;
	
	@Column(unique = false, nullable = false, length = 100)
	private String productName;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="custId")
	@JsonIgnore
	private Customer customer;
	
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss.SSS")
	@JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss.SSS")
	private LocalDateTime createdTime = LocalDateTime.now();
}

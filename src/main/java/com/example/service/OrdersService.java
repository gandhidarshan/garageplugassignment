package com.example.service;

import java.util.List;

import com.example.model.Orders;

public interface OrdersService {
	List<Orders> getOrdersByCustomerId(Long id);
	Orders addOrder(Orders order);
}

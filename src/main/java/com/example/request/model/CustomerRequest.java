package com.example.request.model;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import lombok.Data;

@Data
public class CustomerRequest {

	@NotEmpty(message = "Name is required")
	@Size(max = 100, message = "Name should be max 100")
	private String name;
	
	@NotEmpty(message = "Email is required")
	@Email
	private String email;
}

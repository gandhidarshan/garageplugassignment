package com.example.controller;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.exception.NoDataFoundException;
import com.example.model.Customer;
import com.example.model.Orders;
import com.example.request.model.OrderRequest;
import com.example.response.model.CommonResponse;
import com.example.service.CustomerService;
import com.example.service.OrdersService;
import com.example.utility.CommonUtil;

@RestController
public class OrderController {

	private static Logger log = LoggerFactory.getLogger(OrderController.class.getName());

	public static final BigDecimal ONE_HUNDRED = new BigDecimal(100);
	public static final BigDecimal GOLD_PERCENTAGE = new BigDecimal(10);
	public static final BigDecimal PLATINUM_PERCENTAGE = new BigDecimal(20);
	
	@Autowired
	private OrdersService ordersService;

	@Autowired
	private CustomerService customerService;
	
	@Autowired
	private CommonUtil commonUtill;

	
	@PostMapping("/add/order")
	public ResponseEntity<CommonResponse> createOrder(@Valid @RequestBody OrderRequest orderRequest) {
		log.info("Create Order Calling");
		Customer customer = customerService.getCustomerById(orderRequest.getCustomerId());
		
		if(customer == null)
			throw new NoDataFoundException("No Customer found");
		
		List<Orders> ordersList = ordersService.getOrdersByCustomerId(orderRequest.getCustomerId());
		
		Orders order = new Orders();
		BeanUtils.copyProperties(orderRequest, order);
		order.setCustomer(customer);
		
		BigDecimal discount = new BigDecimal(0.0);
		if(ordersList.size() >= 10 && ordersList.size() < 20) {
			
			discount = orderRequest.getAmount().multiply(GOLD_PERCENTAGE).divide(ONE_HUNDRED);
			order.setDiscountInPercentage(GOLD_PERCENTAGE.doubleValue());
			order.setDiscountAmount(discount);
			
		}else if(ordersList.size() >= 20) {
			
			discount = orderRequest.getAmount().multiply(PLATINUM_PERCENTAGE).divide(ONE_HUNDRED);
			order.setDiscountInPercentage(PLATINUM_PERCENTAGE.doubleValue());
			order.setDiscountAmount(discount);
			
		}
		order.setTotalAmount(orderRequest.getAmount().subtract(discount));
		
		order = ordersService.addOrder(order);
		
		commonUtill.sendEmail(customer.getEmail(), "You have succefully placed order with us.");
		
		Map<String, Object> orderModelMap = new HashMap<>();
		orderModelMap.put("order", order);
		
		log.info("Order placed successfully");
		
		return ResponseEntity.status(HttpStatus.OK).body(
				new CommonResponse(HttpStatus.OK.value(), Boolean.TRUE, "Order placed successfully", orderModelMap));
	}

	
	@GetMapping("/get/orders/{customerId}")
	 public ResponseEntity<CommonResponse> getAllCommentsByPostId(@PathVariable (value = "customerId",required = true) Long customerId) {
		
		Customer customer = customerService.getCustomerById(customerId);
		
		if(customer == null)
			throw new NoDataFoundException("No Customer found");
		
		List<Orders> ordersList = ordersService.getOrdersByCustomerId(customerId);
		if(ordersList.isEmpty())
			throw new NoDataFoundException("No Orders found");
		
		Map<String, Object> orderModelMap = new HashMap<>();
		orderModelMap.put("orders", ordersList);
		
		return ResponseEntity.status(HttpStatus.OK).body(
				new CommonResponse(HttpStatus.OK.value(), Boolean.TRUE, "Customer Orders fetched successfully", orderModelMap));
	}
}

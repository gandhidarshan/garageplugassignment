package com.example.utility;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
public class CommonUtil {
	private static Logger log = LoggerFactory.getLogger(CommonUtil.class.getName());
	
	@Async
	public void sendEmail(String email,String subject) {
		log.info("Send Email : " + email +" Subject :"+subject);
	}
}

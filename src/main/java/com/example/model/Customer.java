package com.example.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@Entity
@Table(name = "customer")
public class Customer implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id", unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long customerId;
	
	@Column(name = "name", unique = false, nullable = false, length = 100)
	private String Name;

	@Column(name = "email", unique = true, nullable = false, length = 100)
	private String email;
	
	@OneToMany(cascade=CascadeType.ALL,mappedBy = "customer",fetch = FetchType.LAZY)
	@JsonIgnore
	private List<Orders> order;
	
}

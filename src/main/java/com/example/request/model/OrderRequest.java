package com.example.request.model;

import java.math.BigDecimal;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;

@Data
public class OrderRequest {

	@NotNull(message = "Customer Id is required")
	private Long customerId;
	
	@NotEmpty(message = "Product Name is required")
	@Size(max = 100, message = "Product Name should be max 100")
	private String productName;
	
	@NotNull(message = "Amount is required")
	@DecimalMin(value = "0.01", message = "Amount should not zero or less")
	private BigDecimal amount;
}

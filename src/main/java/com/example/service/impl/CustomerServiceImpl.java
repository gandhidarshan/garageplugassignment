package com.example.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.model.Customer;
import com.example.repository.CustomerRepository;
import com.example.service.CustomerService;

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private CustomerRepository customerRepository;

	@Override
	public Customer addCustomer(Customer customer) {
		return customerRepository.save(customer);
	}

	@Override
	public Customer getCustomerByEmail(String email) {
		Optional<Customer> customerList = customerRepository.findByEmail(email);
		if(customerList.isPresent())
			return customerList.get();
		
		return null;
	}

	@Override
	public List<Customer> getAllCustomers() {
		return customerRepository.findAll();
	}

	@Override
	public Customer getCustomerById(Long id) {
		Optional<Customer> customer =  customerRepository.findById(id);
		if(customer.isPresent())
			return customer.get();
		return null;
	}

	@Override
	public List<Object[]> getCustomersByTotalTransaction(int total) {
		return customerRepository.findCustomerByTotalOrder(total);
	}
}

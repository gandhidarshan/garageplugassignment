package com.example.exception;

public class AlreadyExistException extends RuntimeException{

	private static final long serialVersionUID = 1L;

	public AlreadyExistException() {
		super();
	}

	public AlreadyExistException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public AlreadyExistException(String message, Throwable cause) {
		super(message, cause);
	}

	public AlreadyExistException(String message) {
		super(message);
	}

	public AlreadyExistException(Throwable cause) {
		super(cause);
	}

}
